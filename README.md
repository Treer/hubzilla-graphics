﻿# Graphics for Hubzilla

This is just a place where I keep graphics I'm working on for [Hubzilla](http://hubzilla.org/).

### License

All this stuff is available under [Hubzilla's license](https://github.com/redmatrix/hubzilla/blob/master/LICENSE)

## Logos

I didn't design Hubzilla's logos.

I've been re-coding some of them to be small and inline, [see the gallery](https://treer.gitlab.io/hubzilla-graphics/example.html)

## Icon fonts

Some of the logo graphics can be easily converted into a web font, or appended to an existing icon font. This would also allow the colour to be specified using `font-color`.

I'll defer this conversion until its needed, since it probably won't be.

## Default profile photos 

  ![flat abstract 1](default_profile_photo/flat_abstract_1/80.png "flat abstract 1") 
  ![flat abstract 2](default_profile_photo/flat_abstract_2/80.png "flat abstract 2")
  ![flat abstract 3](default_profile_photo/flat_abstract_3/80.png "flat abstract 3")
  ![flat abstract 4](default_profile_photo/flat_abstract_4/80.png "flat abstract 4")
  ![flat abstract 4 green](default_profile_photo/flat_abstract_4_green/80.png "flat abstract 4 green")
